// dependencies
const express = require('express');
const mongoose = require('mongoose');

// routes
const taskRoute = require('./routes/taskRoute.js');

// server
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// database connection
// connection to MongoDB Atlas
mongoose.connect('mongodb+srv://Baggam_Rakshan_Tej:eZuCVgYtvzF19Wz4@wdc028-course-booking.hrvki.mongodb.net/b242_to_do?retryWrites=true&w=majority',
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// add the task route
app.use('/tasks', taskRoute);

// Server listening
app.listen(port, ()=>console.log(`Now listening to port ${port}`));
